# klub-cajovna-web

Run locally with `npm run dev`.

## Dependencies

Environment can be loaded from local `.env` file.

```
AUTH_JSON_PATH=/app/secret.json

DATA_SPREADSHEET_ID=16DqCeAtfYMypMva3Blx1wOKWGPxQn9OWV0J7ihFj27E
TEA_SHEET_NAME=CZ_Čaje
TOBACCO_SHEET_NAME=CZ_Tabáky
COFFEE_SHEET_NAME=CZ_Káva
TEA_LOOKUP_SHEET_NAME=CZ_Čaje_Lookup
TOBACCO_LOOKUP_SHEET_NAME=CZ_Tabáky_Lookup
COFFEE_LOOKUP_SHEET_NAME=CZ_Káva_Lookup
HEADER_RANGE=A1:I1
DATA_RANGE=A2:I
LOOKUP_RANGE=A2:B

CALENDAR_SPREADSHEET_ID=1jRXIW0rqI5DgSZNEUy9hZjFid7gV5DC8i34ta57iAns
CALENDAR_DATA_SHEET_NAME=Calendar
CALENDAR_DATA_RANGE=A2:I

BANNER_MESSAGE_SPREADSHEET_ID=1jRXIW0rqI5DgSZNEUy9hZjFid7gV5DC8i34ta57iAns
BANNER_MESSAGE_DATA_SHEET_NAME=Banner
BANNER_MESSAGE_DATA_RANGE=A2:B2

REFRESH_INTERVAL_MS=30000

```

Google API service account `secret.json` is downloaded from GCP project. Service account must have View permissions granted on spreadsheets mentioned in the `.env` file.

```
{
  "type": "service_account",
  "project_id": "klubcajovnamenu",
  "private_key_id": "REDACTED",
  "private_key": "REDACTED",
  "client_id": "113758453514823735115",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/sheets-reader%40klubcajovnamenu.iam.gserviceaccount.com"
}
```

`.env` file is loaded from docker compose. The `secret.json` is mounted inside the container and exists on the server where website runs.
