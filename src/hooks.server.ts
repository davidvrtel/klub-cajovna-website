import type { Handle, HandleServerError } from '@sveltejs/kit';

import { logger } from "$lib/server/logger"

export const handle = (async ({ event, resolve }) => {
    let response = await resolve(event);
    logger.info(`${event.request.method} ${event.url.pathname}${event.url.search} ${response.status}`)
    return response;
}) satisfies Handle;

export const handleError = (({ error, event }) => {
    logger.error(`${event.request.method} ${event.url.pathname}${event.url.search}`)
    logger.error(error as string)
}) satisfies HandleServerError;
