export default function (targetId: string) {
    const el = document.querySelector(targetId);
    if (!el) return;

    el.scrollIntoView({
        behavior: "smooth",
    });
}
