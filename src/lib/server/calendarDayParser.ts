import type { CalendarDay } from "$lib/server/sheetsModels";

export default class {
    private dayOfWeekMap = new Map<number, string>([
        [1, "Pondělí"],
        [2, "Úterý"],
        [3, "Středa"],
        [4, "Čtvrtek"],
        [5, "Pátek"],
        [6, "Sobota"],
        [0, "Neděle"],
    ]);

    private MapBoolean(input?: string): boolean {
        return input === "TRUE";
    }

    private MapDate(input: Date): string {
        return input.toLocaleDateString("cs-CZ", { day: "numeric", month: "numeric", year: "numeric" });
    }

    private MapDayOfWeek(input: Date): string {
        return this.dayOfWeekMap.get(input.getDay()) as string;
    }

    private MapOpenOrDefault(dayOfWeek: number, input?: string): boolean {
        if (input != undefined) {
            return this.MapBoolean(input);
        }

        if (dayOfWeek == 6) {
            return false;
        } else {
            return true;
        }
    }

    private MapStringOrDefault(defaultValue: string, input?: string): string {
        if (typeof input === "string") {
            return input;
        } else {
            return defaultValue;
        }
    }

    private MapTimeFromOrDefault(input?: string): string {
        return this.MapStringOrDefault("18:30", input);
    }

    private MapTimeToOrDefault(input?: string): string {
        return this.MapStringOrDefault("23:00", input);
    }

    private MapTypeOrDefault(input?: string): string {
        return this.MapStringOrDefault("Služba", input);
    }

    private MapDescriptionOrDefault(input?: string): string {
        return this.MapStringOrDefault("", input);
    }

    private MapLinkOrDefault(input?: string): string {
        return this.MapStringOrDefault("", input);
    }

    createCalendarDay(
        date: Date,
        open?: string,
        timeFrom?: string,
        timeTo?: string,
        type?: string,
        description?: string,
        link?: string,
    ): CalendarDay {
        return {
            date: this.MapDate(date),
            dayOfWeek: this.MapDayOfWeek(date),
            open: this.MapOpenOrDefault(date.getDay(), open),
            timeFrom: this.MapTimeFromOrDefault(timeFrom),
            timeTo: this.MapTimeToOrDefault(timeTo),
            type: this.MapTypeOrDefault(type),
            description: this.MapDescriptionOrDefault(description),
            link: this.MapLinkOrDefault(link),
        }
    }
}
