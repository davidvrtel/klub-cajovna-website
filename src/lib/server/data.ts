import type { CalendarData, CalendarWeek, LookupData, ProductData, Banner } from "$lib/server/sheetsModels";

import SheetsClient from "$lib/server/sheetsClient";
import authClient from "$lib/server/googleDriveClient";

import {
    teaLookupSheetName,
    tobaccoLookupSheetName,
    coffeeLookupSheetName,
    teaSheetName,
    tobaccoSheetName,
    coffeeSheetName,
    calendarDataSheetName,
    bannerMessageDataSheetName,
    refreshIntervalMs
} from "$lib/server/envConfig";
import { logger } from "./logger";

export let teaLookup: LookupData | null = null;
export let teaProducts: ProductData | null = null;
export let tobaccoLookup: LookupData | null = null;
export let tobaccoProducts: ProductData | null = null;
export let coffeeLookup: LookupData | null = null;
export let coffeeProducts: ProductData | null = null;
export let calendarData: CalendarData | null = null;
export let banner: Banner | null = null;


const sc = new SheetsClient(authClient);

const loadData = async () => {
    // fetch tea or log error
    logger.warning(`Starting product data fetch`)
    try {
        const tmpTeaLookup = await sc.getLookupData(teaLookupSheetName);
        const tmpTeaProducts = await sc.getProductData(teaSheetName);

        teaLookup = tmpTeaLookup;
        teaProducts = tmpTeaProducts;
        logger.debug(`Fetched ${teaProducts ? teaProducts.length : 0} tea products`)
    } catch (error) {
        logger.error(`Error fetching tea data: ${error}`)
    }

    // fetch tobacco or log error
    try {
        const tmpTobaccoLookup = await sc.getLookupData(tobaccoLookupSheetName);
        const tmpTobaccoProducts = await sc.getProductData(tobaccoSheetName);

        tobaccoLookup = tmpTobaccoLookup;
        tobaccoProducts = tmpTobaccoProducts;
        logger.debug(`Fetched ${tobaccoProducts ? tobaccoProducts.length : 0} tobacco products`)
    } catch (error) {
        logger.error(`Error fetching tobacco data: ${error}`)
    }

    // fetch coffee or log error
    try {
        const tmpCoffeeLookup = await sc.getLookupData(coffeeLookupSheetName);
        const tmpCoffeeProducts = await sc.getProductData(coffeeSheetName);

        coffeeLookup = tmpCoffeeLookup;
        coffeeProducts = tmpCoffeeProducts;
        logger.debug(`Fetched ${coffeeProducts ? coffeeProducts.length : 0} coffee products`)
    } catch (error) {
        logger.error(`Error fetching coffee data: ${error}`)
    }
}

const loadDataJob = setInterval(loadData, refreshIntervalMs);


// calendar
const loadCalendar = async () => {
    const date = new Date();
    const dayOfWeek = date.getUTCDay() == 0 ? 7 : date.getUTCDay() // getting start of current week, but getUTCDay() starts with 0 on sunday
    date.setDate(date.getDate() - dayOfWeek + 1);

    calendarData = await sc.getCalendarData(calendarDataSheetName, date);
    logger.debug(`Fetched calendar events`)
}

const loadCalendarJob = setInterval(loadCalendar, refreshIntervalMs);


// banner
const loadBanner = async () => {
    banner = await sc.getBannerData(bannerMessageDataSheetName);
    logger.debug(`Fetched banner message`)
    logger.debug(`Banner message: ${banner?.message}`)
}

const loadBannerJob = setInterval(loadBanner, refreshIntervalMs);

await loadData();
await loadCalendar();
await loadBanner();
