import { env } from '$env/dynamic/private';

export const authJsonPath = env.AUTH_JSON_PATH;

export const dataSpreadsheetId = env.DATA_SPREADSHEET_ID;
export const teaSheetName = env.TEA_SHEET_NAME;
export const tobaccoSheetName = env.TOBACCO_SHEET_NAME;
export const coffeeSheetName = env.COFFEE_SHEET_NAME;
export const teaLookupSheetName = env.TEA_LOOKUP_SHEET_NAME;
export const tobaccoLookupSheetName = env.TOBACCO_LOOKUP_SHEET_NAME;
export const coffeeLookupSheetName = env.COFFEE_LOOKUP_SHEET_NAME;
// export const headerRange = env.HEADER_RANGE;
export const dataRange = env.DATA_RANGE;
export const lookupRange = env.LOOKUP_RANGE;


export const calendarSpreadsheetId = env.CALENDAR_SPREADSHEET_ID;
export const calendarDataSheetName = env.CALENDAR_DATA_SHEET_NAME;
export const calendarDataRange = env.CALENDAR_DATA_RANGE;

export const bannerMessageSpreadsheetId = env.BANNER_MESSAGE_SPREADSHEET_ID;
export const bannerMessageDataSheetName = env.BANNER_MESSAGE_DATA_SHEET_NAME;
export const bannerMessageDataRange = env.BANNER_MESSAGE_DATA_RANGE;

export const refreshIntervalMs = typeof env.REFRESH_INTERVAL_MS === "undefined" ? 300000 : parseInt(env.REFRESH_INTERVAL_MS);
export const logLevel = typeof env.LOG_LEVEL === "undefined" ? "INFO" : env.LOG_LEVEL;

