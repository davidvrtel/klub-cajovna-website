// import type { JSONClient } from "google-auth-library/build/src/auth/googleauth";
// import type { Compute } from "google-auth-library";
import { GoogleAuth } from "google-auth-library";

// import { google } from "googleapis";

import { authJsonPath } from "$lib/server/envConfig";

const authConfig = {
    keyFilename: authJsonPath,
    scopes: ["https://www.googleapis.com/auth/spreadsheets.readonly"],
};

const auth = new GoogleAuth(authConfig);

export default await auth.getClient();

// export type AuthClient = Compute | JSONClient
