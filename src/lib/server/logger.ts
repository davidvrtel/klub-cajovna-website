import { logLevel } from "$lib/server/envConfig";

enum LogLevel {
    DEBUG = 1,
    INFO = 2,
    WARNING = 4,
    ERROR = 8,
    CRITICAL = 16,
}

type LogLevelName = keyof typeof LogLevel;

class Logger {
    level: LogLevelName;
    levelWeight: LogLevel;

    constructor(level: LogLevelName) {
        this.level = level;

        switch (level) {
            case "DEBUG":
                this.levelWeight = 1;
                break;
            case "INFO":
                this.levelWeight = 2;
                break;
            case "WARNING":
                this.levelWeight = 4;
                break;
            case "ERROR":
                this.levelWeight = 8;
                break;
            case "CRITICAL":
                this.levelWeight = 16;
                break;
            default:
                this.levelWeight = 2;
                break;
        }

        this.debug(`Constructing logger with log level: ${level}`)
    }

    getTimestamp() {
        return new Date().toISOString();
    }

    log(level: LogLevelName, msg: string) {
        let spacesAfter = "";
        let prefix = "";
        const timestamp = this.getTimestamp();

        switch (level) {
            case "DEBUG":
                spacesAfter = "     ";
                prefix = "\x1b[34mDEBUG\x1b[0m";
                break;
            case "INFO":
                spacesAfter = "      ";
                prefix = "INFO";
                break;
            case "WARNING":
                spacesAfter = "   ";
                prefix = "\x1b[33mWARNING\x1b[0m";
                break;
            case "ERROR":
                spacesAfter = "     ";
                prefix = "\x1b[31mERROR\x1b[0m";
                break;
            case "CRITICAL":
                spacesAfter = "  ";
                prefix = "\x1b[31;1mCRITICAL\x1b[0m";
                break;
            default:
                break;
        }

        console.log(`${timestamp} ${prefix}${spacesAfter}${msg}`);
    }

    debug(message: string) {
        if (this.levelWeight <= LogLevel.DEBUG) {
            this.log("DEBUG", message);
        }
    }

    info(message: string) {
        if (this.levelWeight <= LogLevel.INFO) {
            this.log("INFO", message);
        }
    }

    warning(message: string) {
        if (this.levelWeight <= LogLevel.WARNING) {
            this.log("WARNING", message);
        }
    }

    error(message: string) {
        if (this.levelWeight <= LogLevel.ERROR) {
            this.log("ERROR", message);
        }
    }

    critical(message: string) {
        if (this.levelWeight <= LogLevel.CRITICAL) {
            this.log("CRITICAL", message);
        }
    }
}

export const logger = new Logger(logLevel as LogLevelName);
