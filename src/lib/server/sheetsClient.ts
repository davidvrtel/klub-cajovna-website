import type { sheets_v4 } from "@googleapis/sheets";
import { sheets } from "@googleapis/sheets";

import type { ProductData, LookupData, Lookup, Product, CalendarData, Banner } from "$lib/server/sheetsModels";
import { dataSpreadsheetId, calendarSpreadsheetId, dataRange, lookupRange, calendarDataRange, bannerMessageDataRange, bannerMessageSpreadsheetId } from "$lib/server/envConfig";
import CalendarDayParser from "./calendarDayParser";

import { logger } from "$lib/server/logger"

export default class {
  private sheetsClient: sheets_v4.Sheets;
  private cdp: CalendarDayParser = new CalendarDayParser();

  constructor(authClient: any) {
    const sheet = sheets({
      version: "v4",
      auth: authClient,
    });
    this.sheetsClient = sheet;
    logger.debug("Constructing Sheets client")
  }

  // raw data getter
  async getRawData(sheetName: string, range: string, spreadsheetId: string) {
    const result = await this.sheetsClient.spreadsheets.values.get({
      spreadsheetId: spreadsheetId,
      range: `${sheetName}!${range}`,
    });
    logger.debug(`Fetched raw data from ${sheetName}, ${range}, rows: ${result.data.values?.length}`)

    return result.data?.values ?? null;
  }


  // product data getters
  async getProductData(sheetName: string): Promise<ProductData | null> {
    const rawData = await this.getRawData(sheetName, dataRange, dataSpreadsheetId);

    if (rawData === null) {
      return null;
    }

    const result = rawData
      .filter((item: any) => item[0] === "TRUE" && item.length === 10 && item[9].trim().length > 0)
      .map((item: string[]): Product => {
        return {
          highlight: item[1].trim() == "TRUE",
          category: item[2].trim(),
          title: item[3].trim(),
          subtitle: item[4].trim(),
          year: item[5].trim(),
          region: item[6].trim(),
          country: item[7].trim(),
          memberFee: item[8].trim(),
          description: item[9].trim(),
        };
      });

    logger.debug(`Parsed ${result.length} records from product ${sheetName}`)

    return result.length == 0 ? null : result;
  }

  async getLookupData(sheetName: string): Promise<LookupData | null> {
    const rawData = await this.getRawData(sheetName, lookupRange, dataSpreadsheetId);

    if (rawData === null) {
      return null;
    }

    const result = rawData
      .filter((item: any) => item.length === 2)
      .map((item: string[]): Lookup => {
        return {
          title: item[0].trim(),
          hexColor: item[1].trim(),
        };
      });

    logger.debug(`Parsed ${result.length} records from lookup ${sheetName}`)

    return result.length == 0 ? null : result;
  }

  // calendar data getters : Promise<CalendarData>
  async getCalendarData(sheetName: string, dateFrom: Date) {
    let calendarData: CalendarData = [];

    const rawData = await this.getRawData(sheetName, calendarDataRange, calendarSpreadsheetId);

    for (let days = 0; days < 28; days++) {
      const date = new Date(dateFrom);
      date.setDate(dateFrom.getDate() + days);

      const dateString = date.toLocaleDateString("cs-CZ", { day: "2-digit", month: "2-digit", year: "numeric" }).replaceAll(" ", "");

      if (rawData != null) {
        const matchedRow = rawData.find(row => row[0] == dateString)

        if (matchedRow != undefined && matchedRow[8] != "FALSE") {
          const calendarDay = this.cdp.createCalendarDay(
            date,
            matchedRow[2],
            matchedRow[3],
            matchedRow[4],
            matchedRow[5],
            matchedRow[6],
            matchedRow[7],
          );
          calendarData.push(calendarDay);
        } else {
          const calendarDay = this.cdp.createCalendarDay(date);
          calendarData.push(calendarDay);
        }
      } else {
        const calendarDay = this.cdp.createCalendarDay(date);
        calendarData.push(calendarDay);
      }
    }

    logger.debug(`Parsed ${calendarData.length} records from events ${sheetName}`)

    return calendarData;
  }

  async getBannerData(sheetName: string): Promise<Banner | null> {
    const rawData = await this.getRawData(sheetName, bannerMessageDataRange, bannerMessageSpreadsheetId);

    if (rawData === null) {
      return null;
    }

    const result = rawData
      .filter((item: any) => item[0] === "TRUE" && item.length === 2)
      .map((item: string[]): Banner => {
        return {
          message: item[1].trim(),
        };
      });

    logger.debug(`Parsed ${result.length} records from events ${sheetName}`)

    return result.length == 0 ? null : result[0];
  }

}
