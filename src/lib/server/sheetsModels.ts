// Product models
export interface Product {
    highlight: boolean;
    category: string;
    title: string;
    subtitle: string;
    year: string;
    region: string;
    country: string;
    memberFee: string;
    description: string;
}

export interface Lookup {
    title: string;
    hexColor: string;
}

export interface Banner {
    message: string;
}

export type ProductData = Product[];
export type LookupData = Lookup[];

// Calendar models
export interface CalendarDay {
    date: string;
    dayOfWeek: string;
    open: boolean;
    timeFrom: string; // HH:mm format
    timeTo: string; // HH:mm format
    type: string;
    description: string;
    link: string;
}

export type CalendarWeek = CalendarDay[];
export type CalendarData = CalendarDay[];
