import { calendarData } from '$lib/server/data';
import type { PageServerLoad } from './$types';


export const load = (() => {
    return {
        calendar: calendarData
    };
}) satisfies PageServerLoad;

export const prerender = false;
