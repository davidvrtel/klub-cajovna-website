import { coffeeLookup, coffeeProducts } from '$lib/server/data';
import type { PageServerLoad } from './$types';


export const load = (() => {
  return {
    lookup: coffeeLookup,
    products: coffeeProducts
  };
}) satisfies PageServerLoad;

export const prerender = false;
