import { teaLookup, teaProducts } from '$lib/server/data';
import type { PageServerLoad } from './$types';


export const load = (() => {
  return {
    lookup: teaLookup,
    products: teaProducts
  };
}) satisfies PageServerLoad;

export const prerender = false;
