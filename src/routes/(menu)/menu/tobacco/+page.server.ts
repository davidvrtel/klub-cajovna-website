import { tobaccoLookup, tobaccoProducts } from '$lib/server/data';
import type { PageServerLoad } from './$types';


export const load = (() => {
  return {
    lookup: tobaccoLookup,
    products: tobaccoProducts
  };
}) satisfies PageServerLoad;

export const prerender = false;
