import { banner } from '$lib/server/data';
import type { LayoutServerLoad } from './$types';


export const load = (() => {
    return {
        banner: banner
    };
}) satisfies LayoutServerLoad;

export const prerender = false;
